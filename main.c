#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TESTS 4 //количество проводимых тестов

int comparisons = 0; //кол-во сравнений
int exchanges = 0; // кол-во обменов

//---------------------------------
// Функции вывода на экран массивов
//---------------------------------
//Печать массива
void print_array(int* array, int len) {
    for (int i = 0; i < len; i++) {
        printf("%d ", array[i]);
    }
    
    putchar('\n');
}

//функция вывода сгенерированных массивов
void should_print_generated(int** arrays, int len, char key) {
    if (key == 'n') {
        return;
    }
    
    printf("Generated arrays are: \n");
    
    for (int i = 0; i < TESTS; i++) {
        printf("%d) array: ", i + 1);
        print_array(arrays[i], len);
    }
}

//функция вывода отсортированных массивов двумя сортировками
void should_print_sorted(int** selection_arrays, int** heap_arrays, int len, int* selection_comp, int* selection_exch, int* heap_comp, int* heap_exch, char key) {
    if (key == 'n') {
        return;
    }
    
    printf("\nSorted with selection sort arrays: \n");
    printf("---------------------------------- \n");
    
    for (int i = 0; i < TESTS; i++) {
        printf("%d array: ", i + 1);
        print_array(selection_arrays[i], len);
        printf("Compares : %d \n", selection_comp[i]);
        printf("Exchanges : %d \n", selection_exch[i]);
        putchar('\n');
        putchar('\n');
    }
    
    printf("Sorted with heap sort arrays: \n");
    printf("----------------------------- \n");
    
    for (int i = 0; i < TESTS; i++) {
        printf("%d array: ", i + 1);
        print_array(heap_arrays[i], len);
        printf("Compares : %d \n", heap_comp[i]);
        printf("Exchanges : %d \n", heap_exch[i]);
        putchar('\n');
        putchar('\n');
    }
}

//-------------------------------------------------------
// Поменять местами а и b
void swap(int* a, int* b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
    
    exchanges++;
}

// Сравнение двух целых чисел (если: a >|<|= b, то возвращает 1|-1|0 соотвественно)
int compare(int a, int b) {
    comparisons++;
    
    if (a > b) {
        return 1;
    } else if (a < b) {
        return -1;
    }
    
    return 0;
}

//сравнение двух целых чисел -||- для qsort проверки корректности сортировок на сгенерированных тестах
int cmp(const void *a, const void *b) {
    int x = *((int*)a);
    int y = *((int*)b);
    
    if (x > y) {
        return -1;
    } else if (x < y) {
        return 1;
    }
    
    return 0;
}

//функция необходимая для определения корректности отсортированности того или иного массива
int compare_with_qsort(int* qsort_check, int* arrays, int len) {
    for (int i = 0; i < len; i++) {
        if (qsort_check[i] != arrays[i]) {
            return 0;
        }
    }
    
    return 1;
}

//-----------------------------------
// Сортировка методом простого выбора
//-----------------------------------
void straight_selection_sort(int* array, int len) {
    for (int i = 0; i < len - 1; i++) {
        int imax = i;
        int x = array[i];

        for (int j = i + 1; j < len; j++) {
            if (compare(array[j], x) == 1) {
                imax = j;
                x = array[j];
            }
        }

        if (imax != i) {
            swap(&array[imax], &array[i]);
            array[i] = x;
        }
    }
}

//-------------------------
// Пирамидальная сортировка
//-------------------------
// Поддержание свойства не возрастающей пирамиды:
// значения левого и правого сыновей не превышают значения родителя идет так называемое "просеивание элемента"
void sift(int* array, int i, int heap_size) {
    int left = 2 * i + 1; //левый сын
    int right = 2 * i + 2; //правй сын
    int biggest = 0;
    
    if (left < heap_size && compare(array[left], array[i]) == -1) {
        biggest = left;
    } else {
        biggest = i;
    }
    
    if (right < heap_size && compare(array[right], array[biggest]) == -1) {
        biggest = right;
    }
    
    if (biggest != i) {
        swap(&array[i], &array[biggest]);
        sift(array, biggest, heap_size);
    }
}

// Функция пирамидальной сортировки
void heap_sort(int* array, int len) {
    for (int i = len / 2 - 1; i >= 0; i--) { // Построение пирамиды
        sift(array, i, len);
    }

    for (int i = len - 1; i > 0; i--) {
        swap(&array[0], &array[i]); // Текущий минимальный элемент в конец
        sift(array, 0, i); // Восстанавливаем пирамиду в оставшемся массиве
    }
}

//---------------------------
// Функция генерации массивов
//---------------------------
// Генерация массивов
int* generate_arrays(int len, int j) {
    int* array = (int*)malloc(len * sizeof(int));
    
    if (j == 3 || j == 4) { //генерация случайного массива
        for (int i = 0; i < len; i++) {
            array[i] = rand() * rand() * rand();
        }
    }
    else if (j == 1) {
        for (int i = len - 1; i >= 0; i--) { // Генерация массива, упорядоченного по невозврастанию
            array[i] = len - i;
        }
    }
    else if (j == 2) { // Генерация массива, упорядоченного по возрастанию (худший случай)
        for (int i = 1; i <= len; i++) {
            array[i - 1] = i;
        }
    }
    
    return array;
}

//---------------------------
// Функция печати результатов
//---------------------------
void print_results(int* selection_comp, int* selection_exch, int* heap_comp, int* heap_exch, int len) {
    float mid_comp = (selection_comp[0] + selection_comp[1] + selection_comp[2] + selection_comp[3]) / TESTS;
    float mid_exch = (selection_exch[0] + selection_exch[1] + selection_exch[2] + selection_exch[3]) / TESTS;

    printf("Результаты работы сортировок:\n\nМетод простого выбора для %d элементов", len);
    printf("\n___________________________________________________________________________\n");
    printf("|массив     |  1  | 2   | 3   | 4   | среднее значение \n");
    printf("___________________________________________________________________________\n");
    printf("|сравнения  | %d  | %d  | %d  | %d  | %f     \n", selection_comp[0], selection_comp[1], selection_comp[2], selection_comp[3], mid_comp);
    printf("___________________________________________________________________________\n");
    printf("|перемещения| %d  | %d  | %d  | %d  | %f     \n", selection_exch[0], selection_exch[1], selection_exch[2], selection_exch[3], mid_exch);
    printf("___________________________________________________________________________\n\n");

    mid_comp = (heap_comp[0] + heap_comp[1] + heap_comp[2] + heap_comp[3]) / TESTS;
    mid_exch = (heap_exch[0] + heap_exch[1] + heap_exch[2] + heap_exch[3]) / TESTS;

    printf("Пирамидальная сортировка для %d элементов", len);
    printf("\n___________________________________________________________________________\n");
    printf("|массив     | 1  | 2  | 3  | 4  | среднее значение \n");
    printf("___________________________________________________________________________\n");
    printf("|сравнения  | %d | %d | %d | %d | %f               \n", heap_comp[0], heap_comp[1], heap_comp[2], heap_comp[3], mid_comp);
    printf("___________________________________________________________________________\n");
    printf("|перемещения| %d | %d | %d | %d | %f               \n", heap_exch[0], heap_exch[1], heap_exch[2], heap_exch[3], mid_exch);
    printf("___________________________________________________________________________\n");
}

void print_i_test(int* array, int* array_copy, int i) {
    printf("%d) test: ", i);
    print_array(array, 5);
    printf("Selection sort: ");
    straight_selection_sort(array, 5);
    print_array(array, 5);
    printf("Heap sort: ");
    heap_sort(array_copy, 5);
    print_array(array_copy, 5);
}

void default_tests(char key) {
    if (key == 'n') {
        return;
    }
    
    int i = 1;
    int array[] = {1, 1, 2, 3, 4};
    int array_copy[] = {1, 1, 2, 3, 4};
    
    print_i_test(array, array_copy, i++);
    
    int array_2[] = {150, 149, 148, 147, 146};
    int array_copy_2[] = {150, 149, 148, 147, 146};
    
    print_i_test(array_2, array_copy_2, i++);
    
    int array_3[] = {-991, -18888, 1727, 46, -78};
    int array_copy_3[] = {-991, -18888, 1727, 46, -78};
    
    print_i_test(array_3, array_copy_3, i++);
}

void qsort_checking(int** qsort_check, int** selection_arrays, int** heap_arrays, int len) {
    for (int i = 0; i < TESTS; i++) {
        if (compare_with_qsort(qsort_check[i], selection_arrays[i], len)) {
            printf("Selection sort: %d array sorted correct\n", i + 1);
        }
        else {
            printf("Selection sort: %d array sorted incorrect !!!\n", i + 1);
        }

        if (compare_with_qsort(qsort_check[i], heap_arrays[i], len)) {
            printf("Heap sort: %d array sorted correct\n", i + 1);
        }
        else {
            printf("Heap sort: %d array sorted incorrect !!!\n", i + 1);
        }
        
        putchar('\n');
    }
}
int main(void) {
    srand(time(NULL));
    
    printf("Сортировки: метод простого выбора и пирамидальная сортировка для целочисленных массивов. Необходимо упорядочить в порядке невозрастания.\n");
    printf("Хотите ли вы проверить корректность работы сортировок на 3-х встроенных тестах? Введите y/n: ");
    
    default_tests(getchar());
    exchanges = 0;
    comparisons = 0;
    
    printf("Enter array size: = ");

    int len;
    scanf("%d", &len);
    
    int** selection_arrays = (int**)malloc(TESTS * sizeof(int*));
    int** heap_arrays = (int**)malloc(TESTS * sizeof(int*));
    int** qsort_check = (int**)malloc(TESTS * sizeof(int*));
    
    for (int i = 0; i < TESTS; i++) {
        selection_arrays[i] = (int*)malloc(len * sizeof(int));
        heap_arrays[i] = (int*)malloc(len * sizeof(int));
        qsort_check[i] = (int*)malloc(len * sizeof(int));
    }
    
    printf("Generating arrays...\n");
    
    for (int j = 0; j < TESTS; j++) {
        selection_arrays[j] = generate_arrays(len, j + 1);
        memcpy(heap_arrays[j], selection_arrays[j], len * sizeof(int));
        memcpy(qsort_check[j], selection_arrays[j], len * sizeof(int));
    }
    
    printf("Arrays have been generated\nDo you want see generated arrays ? Enter y/n: ");
    
    getchar();
    char key = getchar();
    
    should_print_generated(selection_arrays, len, key);
    
    // Количество сравнений и обменов для каждой из сортировок
    int selection_comp[TESTS];
    int selection_exch[TESTS];

    int heap_comp[TESTS];
    int heap_exch[TESTS];
    
    printf("Do you want checking the correctness of the sorts with qsort ? Enter y/n: ");
    getchar();
    key = getchar();
    
    printf("\nSorting arrays...\n");
    
    for (int i = 0; i < TESTS; i++) {
        straight_selection_sort(selection_arrays[i], len);
        
        selection_comp[i] = comparisons;
        comparisons = 0;
        
        selection_exch[i] = exchanges;
        exchanges = 0;
    }
    
    for (int i = 0; i < TESTS; i++) {
        heap_sort(heap_arrays[i], len);
        
        heap_comp[i] = comparisons;
        comparisons = 0;
        
        heap_exch[i] = exchanges;
        exchanges = 0;
    }
    
    if (key != 'n') {
        for (int i = 0; i < TESTS; i++) {
            qsort(qsort_check[i], len, sizeof(int), cmp);
        }
        
        qsort_checking(qsort_check, selection_arrays, heap_arrays, len);
    }
    
    printf("Arrays have been sorted\nDo you want see sorted arrays ? Enter y/n: ");
    
    getchar();
    key = getchar();
    should_print_sorted(selection_arrays, heap_arrays, len, selection_comp, selection_exch, heap_comp, heap_exch, key);
    
    print_results(selection_comp, selection_exch, heap_comp, heap_exch, len);
    
    for (int i = 0; i < TESTS; i++) {
        free(heap_arrays[i]);
        free(selection_arrays[i]);
        free(qsort_check[i]);
    }
    
    free(selection_arrays);
    free(heap_arrays);
    free(qsort_check);
    
    return 0;
}
